# Python script for normalizing string.


## Usage

### Clone the repository
```bash
git clone https://gitlab.com/youpsla/normalize-string
```

### cd to the normalize-string directory
```bash
cd normalize-string
```

### Running the script:
```bash
docker build -t normalize_string .; docker run -it normalize_string
```