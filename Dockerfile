FROM python:3.12.1-slim-bookworm

WORKDIR /code

COPY src/normalize_string.py .

ENTRYPOINT [ "python", "normalize_string.py" ]
