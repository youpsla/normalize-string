from typing import List, Literal
import sys

# TODO:
#  - Manage last caracter.
#  - Manage reserved python caracter

SEPARATOR_CHARS_BY_PRIORITY = [" ", ".", "#", "_"]

PREDEFINED_INPUTS_STRINGS = [
    ("", ""),
    ("pierre..   _Louis   guhur", "Pierre Louis Guhur"),
    ("Pierre#   _Louis   guhur", "Pierre Louis Guhur"),
    ("               ", " "),
    ("aaaaaaaaaaa", "Aaaaaaaaaaa"),
]

def get_char_type(c: str) -> Literal["alpha", "separator"]:
    """
    Detect the type of caracter s.

    Args:
        s: The caracter to be checked.

    Returns:
        A string representing the type of the caracter.
    """
    return "alpha" if c .isalpha() else 'separator'


def get_separator_referential_index(c: str) -> int:
    """Test if given separator exist in referential.

    Args:
        c: THe caracter searched in referential
    Returns:
        An int representing the index of the caracter.
    """
    try:
        return SEPARATOR_CHARS_BY_PRIORITY.index(c)
    except ValueError:
        sys.exit(f"""Unknow separator "{c}" in input string. Only those saparators are accepted: {', '.join(SEPARATOR_CHARS_BY_PRIORITY)}""")


def set_separator(s: str) -> str:
    """ Find the separator with the lowest index in SEPARATOR_CHARS_BY_PRIORITY

    Args:
        s: The string of separator(s)
    Returns:
        str: The separator
    """
    min_index = s.index(min(s, key=get_separator_referential_index))
    return s[min_index]


def normalize_string(s: str, total_result = '') -> str:
    """
    Normalizes a string by:
    - Capitalizing alpha consecutives caracters
    - Keeping the seaprator with the lowest index in referential.

    Args:
        s: The string to be normalized.
        result: An optional list to store the normalized characters. If not provided, a new list will be created.

    Returns:
        total_result: string

    """

    len_s = len(s)

    # Empty string case
    if len_s == 0:
        return ''

    results: List = []
    
    for c_idx, c in enumerate(s, start = 1):
        if c_idx == 1:
            results.append(c)
            continue
        
        current_char_type = get_char_type(c)
        preceding_char_type = get_char_type(s[c_idx-2])

        if preceding_char_type == current_char_type:
            results.append(c)
        else:
            c_idx -= 1
            break

    # Formatting string or separator to be added to total_result
    if preceding_char_type == "alpha":
        total_result +=  ''.join(results).capitalize()
    
    if preceding_char_type == "separator":
        total_result += set_separator(results)

    s = str(s[c_idx:])

    if len(s)<= 0:
        # Base case
        return total_result
    else:
        # Recursion
        return '' + str(normalize_string(s, total_result))


def cli():

    # Display welcome message
    print("\nWelcome to string normalizer.\n")

    # Display predefinder choices
    for idx, s in enumerate(PREDEFINED_INPUTS_STRINGS, start=1):
        print(f"{idx}) {s[0]}")
    

    choice_idx = int(input("\nEnter your choice: "))
    choice_str = PREDEFINED_INPUTS_STRINGS[choice_idx-1][0]

    print(f"\nProcessing {choice_idx}: {choice_str}")

    return choice_str


if __name__ == "__main__":

    usr_input = cli()
    
    total_result = normalize_string(usr_input)

    print("\n########## Result ##########")
    print(total_result)
    print("############################")

